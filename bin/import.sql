insert into cliente(id,nome) values(1, 'Diego');
insert into cliente(id,nome) values(2, 'Emiliano');
insert into cliente(id,nome) values(3, 'Francisca');
insert into cliente(id,nome) values(4, 'Joao');
insert into cliente(id,nome) values(5, 'Sirineo');

insert into PontoTuristico(id,descricao,endereco,duracao) values(1,'igreja de sao bento','centro',   30);
insert into PontoTuristico(id,descricao,endereco,duracao) values(2,'encontro das aguas' ,'distrito', 60);
insert into PontoTuristico(id,descricao,endereco,duracao) values(3,'teatro amazonas'    ,'centro',   80);
insert into PontoTuristico(id,descricao,endereco,duracao) values(4,'ponte rio Negro'    ,'Ponta negra',60);
insert into PontoTuristico(id,descricao,endereco,duracao) values(5,'palacio Negro'      ,'centro',60);
insert into PontoTuristico(id,descricao,endereco,duracao) values(6,'praca 14'           ,'centro',60);
insert into PontoTuristico(id,descricao,endereco,duracao) values(7,'museu do indio'     ,'centro',60);

insert into GUIA (nome, cpf) values ('Emiliano', '12345');
insert into GUIA (nome, cpf) values ('Diego', '67890');
insert into GUIA (nome, cpf) values ('Fran', 'ABCDEF');
insert into GUIA (nome, cpf) values ('Joao', 'WXYZ');

insert into MOTORISTA (nome, cpf, carteira, datanascimento) values ('Pedro',    '759.311.431-73', '00000', '1985-04-01');
insert into MOTORISTA (nome, cpf, carteira, datanascimento) values ('Jose',     '855.805.614-58', '00000', '1974-12-25');
insert into MOTORISTA (nome, cpf, carteira, datanascimento) values ('Afonso',   '630.313.488-26', '00000', '1983-03-08');
insert into MOTORISTA (nome, cpf, carteira, datanascimento) values ('Maria',    '432.537.752-24', '00000', '1969-10-14');
insert into MOTORISTA (nome, cpf, carteira, datanascimento) values ('Anastasia','168.250.093-46', '00000', '1972-07-22');

insert into VEICULO (placa, modelo, fabricante, locacaomaxima, consumomedio, capacidadetanque) values ('XYZ-1234', 'onibus',      'merceds', 45, 0.7, 120);
insert into VEICULO (placa, modelo, fabricante, locacaomaxima, consumomedio, capacidadetanque) values ('IJK-9876', 'minivan',     'kia',     25, 1.3, 40);
insert into VEICULO (placa, modelo, fabricante, locacaomaxima, consumomedio, capacidadetanque) values ('QWE-4422', 'micro-onibus','ford',    32, 1.2, 40);
insert into VEICULO (placa, modelo, fabricante, locacaomaxima, consumomedio, capacidadetanque) values ('MNO-2960', 'kombi',       'volkwagen',9, 2.8, 12);

insert into Roteiro (id, duracao_total, nome) values(1, 120,'Belle Epoque');
insert into Passeio (id, data, estado , precoUnitario, roteiro_id) values(1, '2012-12-25',0,1500,1);

insert into Roteiro_PontoTuristico (roteiro_id, pontos_id) values (1, 1)
insert into Roteiro_PontoTuristico (roteiro_id, pontos_id) values (1, 3)
insert into Roteiro_PontoTuristico (roteiro_id, pontos_id) values (1, 5)
insert into Roteiro_PontoTuristico (roteiro_id, pontos_id) values (1, 6)

insert into User (id, userName, password) values (1, 'Filho',   '1234');
insert into User (id, userName, password) values (2, 'Firmino', '12345');
insert into User (id, userName, password) values (3, 'Lins',    '12345');
insert into User (id, userName, password) values (4, 'Martinez','12345');