package br.fucapi.es.poo.solarium.view;

import java.awt.EventQueue;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

import br.fucapi.es.poo.solarium.controller.PontoTuristicoControlador;
import br.fucapi.es.poo.solarium.entity.PontoTuristico;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PontoTuristicoMenu extends JFrame {

	private JPanel contentPane;
	private DefaultListModel<PontoTuristico> model;
	private PontoTuristicoControlador controler = new PontoTuristicoControlador();
	
	public PontoTuristicoMenu() {
		setTitle("Pontos Tur\u00EDsticos");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		model = new DefaultListModel();
		Set<PontoTuristico> pontos = controler.buscaPontos();
		
		int i = 0;
		
		for (PontoTuristico ponto : pontos) {
			model.add(i, ponto);
			i++;
		}
		
		final JList list = new JList(model);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setBounds(26, 42, 384, 121);
		contentPane.add(list);
		
		JButton btnVer = new JButton("Ver");
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = list.getSelectedIndex();
				if(model.size() > 0 && index >= 0){
					PontoTuristico ponto = model.get(index);
					String texto = ponto.getDescricao() + "\nDura��o: " + ponto.getDuracao() + " minutos\nEndere�o: " + ponto.getEndereco();
					JOptionPane.showMessageDialog(null, texto);
				} else {
					JOptionPane.showMessageDialog(null, "Voc� deve selecionar um ponto Turistico!");
				}
			}
		});
		btnVer.setBounds(26, 174, 89, 23);
		contentPane.add(btnVer);
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = list.getSelectedIndex();
				if(model.size() > 0 && index >= 0) {
					PontoTuristicoForm tela = new PontoTuristicoForm(model,false,model.get(index));
					tela.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Voc� deve selecionar um ponto Turistico!");
				}
			}
		});
		btnAlterar.setBounds(168, 174, 89, 23);
		contentPane.add(btnAlterar);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				if(model.size() > 0 && index >= 0 && JOptionPane.showConfirmDialog(null, "Deseja mesmo excluir?") == 0) {
					PontoTuristico ponto = model.get(index);
					controler.excluirPonto(ponto);
					model.remove(index);
				} else {
					JOptionPane.showMessageDialog(null, "Voc� deve selecionar um ponto Turistico!");
				}
			}
		});
		btnExcluir.setBounds(295, 174, 89, 23);
		contentPane.add(btnExcluir);
		
		JLabel lblPontosCadastrados = new JLabel("Pontos cadastrados:");
		lblPontosCadastrados.setBounds(26, 17, 213, 14);
		contentPane.add(lblPontosCadastrados);
		
		JButton btnCadastrarNovo = new JButton("Cadastrar novo");
		btnCadastrarNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PontoTuristicoForm tela = new PontoTuristicoForm(model,true,null);
				tela.setVisible(true);
			}
		});
		btnCadastrarNovo.setBounds(128, 228, 167, 23);
		contentPane.add(btnCadastrarNovo);
	}
}
