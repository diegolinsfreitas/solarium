package br.fucapi.es.poo.solarium.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import br.fucapi.es.poo.solarium.controller.PasseioControlador;
import br.fucapi.es.poo.solarium.entity.Guia;
import br.fucapi.es.poo.solarium.entity.Motorista;
import br.fucapi.es.poo.solarium.entity.Passeio;
import br.fucapi.es.poo.solarium.entity.Veiculo;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/*import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;*/

/*import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
<<<<<<< HEAD
//import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;
=======
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;*/

public class ProgramacaoPasseioFormulario
	extends JFrame implements ActionListener {
	
	private Passeio passeio;
	private JButton btSalvar, btCancelar;
	private JComboBox<Guia>      cbGuia;
	private JComboBox<Motorista> cbMotorista;
	private JComboBox<Veiculo>   cbVeiculo;
	
	public ProgramacaoPasseioFormulario(
			Passeio passeio,
			List<Guia> guiaDisponivel,
			List<Motorista> motoristaDisponiveis,
			List<Veiculo> veiculoDisponivel) {
		
		this.passeio = passeio;
		System.out.println(passeio.toString());
		
		JPanel content = new JPanel();
		
/*		content.setLayout(new FormLayout(new ColumnSpec[] {
		/*content.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("70px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("93px"),
				ColumnSpec.decode("35px"),
				ColumnSpec.decode("95px"),},
			new RowSpec[] {
				RowSpec.decode("20px"),
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("23px"),}));*/
		/*content.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("28px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("28px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("52px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("28px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("40px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("28px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("52px"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("23px"),
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("23px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));*/
		
		JLabel lbGuia      = new JLabel("Guia: ");
		content.add(lbGuia, "2, 2, 3, 1, left, center");
		
		this.setTitle("Montar Programacao");
		this.setContentPane(content);
		
		int index = -1;
		Guia guia = passeio.getGuia();
		for(int i = 0; guia != null && i < guiaDisponivel.size(); i++) {
			if (guia.getId() == guiaDisponivel.get(i).getId()) {
				index = i;
				break;
			}
		}
		
		index = -1;
		Motorista motorista = passeio.getMotorista(); 
		for(int i = 0; motorista != null && i < motoristaDisponiveis.size(); i++) {
			if (motorista.getId() == motoristaDisponiveis.get(i).getId()) {
				index = i;
				break;
			}
		}
		
		index = -1;
		Veiculo veiculo = passeio.getVeiculo();
		for(int i = 0; veiculo != null && i < veiculoDisponivel.size(); i++) {
			if (veiculo.getId() == veiculoDisponivel.get(i).getId()) {
				index = i;
				break;
			}
		}
		cbGuia = new JComboBox(guiaDisponivel.toArray());
		content.add(cbGuia, "6, 2, 9, 1, fill, fill");
		cbGuia.setSelectedIndex(index);
		JLabel lbMotorista = new JLabel("Motorista: ");
		content.add(lbMotorista, "2, 4, 3, 1, left, center");
		cbMotorista = new JComboBox(motoristaDisponiveis.toArray());
		
		content.add(cbMotorista, "6, 4, 9, 1, fill, center");
		cbMotorista.setSelectedIndex(index);
		JLabel lbVeiculo   = new JLabel("Veiculo: ");
		content.add(lbVeiculo, "2, 6, 3, 1, left, center");
		cbVeiculo = new JComboBox(veiculoDisponivel.toArray());
		
		
		content.add(cbVeiculo, "6, 6, 9, 1, fill, center");
		cbVeiculo.setSelectedIndex(index);
		
		btSalvar = new JButton("Salvar");
		btSalvar.addActionListener(this);
		
		content.add(btSalvar, "6, 8, 3, 1, left, top");
		btCancelar = new JButton("Cancelar");
		btCancelar.addActionListener(this);
		content.add(btCancelar, "10, 8, 3, 1, right, top");
		
		this.setSize(320, 202);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btSalvar) {
			int option = JOptionPane.showConfirmDialog(this, "Deseja salvar alterações?");
			if (option != JOptionPane.CANCEL_OPTION) {
				if (option == JOptionPane.OK_OPTION) {
					passeio.setGuia((Guia) cbGuia.getSelectedItem());
					passeio.setMotorista((Motorista) cbMotorista.getSelectedItem());
					passeio.setVeiculo((Veiculo) cbVeiculo.getSelectedItem());
					
					PasseioControlador p = new PasseioControlador();
					p.salvar(passeio);
				}
				this.setVisible(false);
			}
		}
		else if (e.getSource() == btCancelar) {
			int option = JOptionPane.showConfirmDialog(this, "Deseja sair?, As modificações não serão salvas", "Cancelar", JOptionPane.YES_NO_OPTION);
			if (option == JOptionPane.YES_OPTION) {
				this.setVisible(false);
			}
		}
		
	}
	
}
