package br.fucapi.es.poo.solarium.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import br.fucapi.es.poo.solarium.controller.PontoTuristicoControlador;
import br.fucapi.es.poo.solarium.entity.PontoTuristico;

public class PontoTuristicoForm extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JButton botao;

	/**
	 * Create the frame.
	 */
	public PontoTuristicoForm(final DefaultListModel<PontoTuristico> model, boolean cadastrar, final PontoTuristico ponto) {
		setTitle("Criar/Editar Ponto Turistico");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(137, 23, 270, 69);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(137, 118, 141, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(137, 161, 141, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblDescrio = new JLabel("Descri\u00E7\u00E3o:");
		lblDescrio.setBounds(10, 35, 73, 14);
		contentPane.add(lblDescrio);
		
		JLabel lblDurao = new JLabel("Dura\u00E7\u00E3o:");
		lblDurao.setBounds(10, 121, 89, 14);
		contentPane.add(lblDurao);
		
		JLabel lblLocalizao = new JLabel("Localiza\u00E7\u00E3o:");
		lblLocalizao.setBounds(10, 164, 73, 14);
		contentPane.add(lblLocalizao);
		if(cadastrar) {
			cadastrar(model);
		} else {
			alterar(ponto);
		}
	}
	
	private void cadastrar(final DefaultListModel<PontoTuristico> model) {
		botao = new JButton("Cadastrar");
		botao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(validar()) {
					PontoTuristicoControlador controler = new PontoTuristicoControlador();
					PontoTuristico ponto = controler.cadastrarPonto(textField.getText(), textField_2.getText(), Integer.parseInt(textField_1.getText()));
					model.add(model.size(), ponto);
					dispose();
				}
			}
		});
		botao.setBounds(137, 210, 141, 23);
		contentPane.add(botao);
	}
	
	private void alterar(final PontoTuristico ponto) {
		textField.setText(ponto.getDescricao());
		textField_1.setText(""+ponto.getDuracao());
		textField_2.setText(ponto.getEndereco());
		botao = new JButton("Alterar");
		botao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(validar()) {
					PontoTuristicoControlador controler = new PontoTuristicoControlador();
					controler.alterarPonto(ponto, textField.getText(), textField_2.getText(), Integer.parseInt(textField_1.getText()));
					dispose();
				}
			}
		});
		botao.setBounds(137, 210, 141, 23);
		contentPane.add(botao);	
	}
	
	
	public boolean validar() {
		int duracao = 0;
		try {
			duracao = Integer.parseInt(textField_1.getText());
		} catch (Exception e) {
			duracao = 0;
		}
		
		if(textField.getText().length() > 0 && textField_2.getText().length() > 0 && duracao > 0 ) {
			return true;
		} else {
			return false;
		}
	}
}
