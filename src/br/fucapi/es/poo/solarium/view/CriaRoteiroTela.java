package br.fucapi.es.poo.solarium.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import br.fucapi.es.poo.solarium.controller.PontoTuristicoControlador;
import br.fucapi.es.poo.solarium.controller.RoteiroController;
import br.fucapi.es.poo.solarium.entity.PontoTuristico;
import br.fucapi.es.poo.solarium.entity.Roteiro;

import javax.swing.ListModel;
import javax.swing.JLabel;

public class CriaRoteiroTela extends JFrame {

	private JPanel contentPane;
	private DefaultListModel<PontoTuristico> model = new DefaultListModel();
	private DefaultListModel<PontoTuristico> model2 = new DefaultListModel();
	private DefaultListModel<Roteiro> modelView = new DefaultListModel();
	private PontoTuristicoControlador controlerPontoTuristico = new PontoTuristicoControlador();
	private RoteiroController controlerRoteiro = new RoteiroController();
	private JTextField textField;
	private JButton botao_cadastrar;
	private JList list;
	private JList list_1;

	public CriaRoteiroTela(final DefaultListModel<Roteiro> model3, boolean cadastrar, Roteiro roteiro) {
		this.modelView = model3;
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 566, 481);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		inicializaListaDireita(roteiro);
		inicializaListaEsquerda();
		
		JButton botao_escolher = new JButton(">>");
		botao_escolher.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = list.getSelectedIndex();
				if(index >= 0) {
					model2.add(model2.size(), model.get(index));
				}
			}
		});
		botao_escolher.setBounds(240, 64, 71, 23);
		contentPane.add(botao_escolher);		
		
		JButton botao_excluir = new JButton("<<");
		botao_excluir.setBounds(240, 111, 71, 23);
		botao_excluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = list.getSelectedIndex();
				if(index >= 0) {
					model2.remove(list_1.getSelectedIndex());
				}				
			}
		});
		contentPane.add(botao_excluir);
		
		textField = new JTextField();
		textField.setBounds(59, 225, 197, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblDurao = new JLabel("Nome:");
		lblDurao.setBounds(60, 200, 119, 14);
		contentPane.add(lblDurao);
		
		if(cadastrar) {
			cadastrar();
		} else {
			alterar(roteiro);
		}
	}
	
	private void cadastrar() {
		botao_cadastrar = new JButton("Finalizar");
		botao_cadastrar.setBounds(343, 224, 89, 23);
		botao_cadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField.getText().length() > 0 ) {
					Roteiro roteiro = controlerRoteiro.cadastrarRoteiro(model2,textField.getText());
					modelView.addElement(roteiro);
					dispose();
				}
			}
		});
		contentPane.add(botao_cadastrar);
	}
	
	private void alterar(final Roteiro roteiro) {
		botao_cadastrar = new JButton("Finalizar");
		botao_cadastrar.setBounds(343, 224, 89, 23);
		botao_cadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField.getText().length() > 0 ) {
					controlerRoteiro.alterarRoteiro(roteiro, model2,textField.getText());
					dispose();
				}
			}
		});
		contentPane.add(botao_cadastrar);
	}
	
	
	private void inicializaListaDireita(Roteiro roteiro) {
		if(!roteiro.getPontos().isEmpty()) {
			List<PontoTuristico> pontos = roteiro.getPontos();
			int i = 0;
			for (PontoTuristico ponto : pontos) {
				model2.add(i, ponto);
				i++;
			}
		} 
		list_1 = new JList(model2);
		list_1.setBounds(330, 41, 220, 136);
		list_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		contentPane.add(list_1);
	}
	
	
	private void inicializaListaEsquerda() {
		Set<PontoTuristico> pontos = controlerPontoTuristico.buscaPontos();
		int i = 0;
		for (PontoTuristico ponto : pontos) {
			model.add(i, ponto);
			i++;
		}
		list = new JList(model);
		list.setBounds(10, 41, 220, 136);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		contentPane.add(list);
	}
}
