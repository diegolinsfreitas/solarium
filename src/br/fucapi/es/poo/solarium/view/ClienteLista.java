package br.fucapi.es.poo.solarium.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.fucapi.es.poo.solarium.controller.ClienteController;
import br.fucapi.es.poo.solarium.entity.Cliente;

public class ClienteLista extends JDialog {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	private ArrayList<Cliente> listaClientes;

	/**
	 * Create the frame.
	 */
	public ClienteLista() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setText("");
		textField.setBounds(66, 45, 366, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNovo = new JButton("Novo");
		btnNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClienteFormulario clienteFormulario = new ClienteFormulario();
				clienteFormulario.setVisible(true);
				clienteFormulario.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent arg0) {
						ClienteLista.this.carregarTabela();
					}
				});
			}
		});
		btnNovo.setBounds(10, 11, 91, 23);
		contentPane.add(btnNovo);
		
		JLabel lblPesquisa = new JLabel("Pesquisa");
		lblPesquisa.setBounds(10, 48, 46, 14);
		contentPane.add(lblPesquisa);
		
		table = new JTable();
		table.setBounds(10, 73, 422, 189);
		contentPane.add(table);
		this.carregarTabela();
	}
	
	private void carregarTabela(){
		Collection<Cliente> clientes = new ClienteController().buscaClientes();
		this.listaClientes = new ArrayList<Cliente>(clientes);
		DefaultTableModel aModel = new DefaultTableModel() {
			// setting the jtable read only

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		// setting the column name
		Object[] tableColumnNames = new Object[2];
		tableColumnNames[0] = "ID";
		tableColumnNames[1] = "Nome";

		aModel.setColumnIdentifiers(tableColumnNames);


		Object[] objects = new Object[2];
		Iterator<Cliente> itClientes = this.listaClientes.iterator();
		// populating the tablemodel
		while (itClientes.hasNext()) {
			Cliente cliente = itClientes.next();
			objects[1] = cliente.getNome();
			objects[0] = cliente.getId();

			aModel.addRow(objects);
		}

		// binding the jtable to the model
		this.table.setModel(aModel);
	}
}
