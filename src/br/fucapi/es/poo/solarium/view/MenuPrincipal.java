package br.fucapi.es.poo.solarium.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import br.fucapi.es.poo.solarium.controller.PasseioControlador;
import br.fucapi.es.poo.solarium.entity.Passeio;
import br.fucapi.es.poo.solarium.repositorio.PasseioRepositorio;

public class MenuPrincipal extends JFrame implements ActionListener {

	private JButton btCliente, btReserva, btRoteiro, btCancelarReserva, btPontoTuristico;;
	private JMenuItem miCliente, miReserva, miRoteiro, miCancelarReserva, miPontoTuristico;
	
	public MenuPrincipal() {
		btCliente = new JButton("Gerenciar Cliente");
		btReserva = new JButton("Gerenciar Reserva");
		btRoteiro = new JButton("Gerenciar Roteiro");

                btCancelarReserva = new JButton("Cancelar Reserva");

		btPontoTuristico = new JButton ("Gerenciar Pontos Turisticos");

		
		btCliente.addActionListener(this);
		btReserva.addActionListener(this);
		btRoteiro.addActionListener(this);

                btCancelarReserva.addActionListener(this);

		btPontoTuristico.addActionListener(this);

		
		JPanel content = new JPanel();
		content.add(btCliente);
		content.add(btReserva);
		content.add(btRoteiro);

                content.add(btCancelarReserva);

		content.add(btPontoTuristico);

		
		miCliente = new JMenuItem("cliente");
		miReserva = new JMenuItem("reserva");
		miRoteiro = new JMenuItem("roteiro");
                miCancelarReserva = new JMenuItem("Cancelar Reserva");
		miPontoTuristico = new JMenuItem("ponto turistico");
		
		miCliente.addActionListener(this);
		miReserva.addActionListener(this);
		miRoteiro.addActionListener(this);
                miCancelarReserva.addActionListener(this);
		miPontoTuristico.addActionListener(this);
		
		JMenuItem gerenciar = new JMenu("Gerenciar");
		gerenciar.add(miCliente);
		gerenciar.add(miReserva);
		gerenciar.add(miRoteiro);
                gerenciar.add(miCancelarReserva);
		gerenciar.add(miPontoTuristico);
		
		JMenuBar menu = new JMenuBar();
		menu.add(gerenciar);
		
		this.setTitle("Solaris Turismo - Menu");
		this.setJMenuBar(menu);
		this.setContentPane(content);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(360, 240);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		if (source == btCliente || source == miCliente) {
			ClienteLista cl = new ClienteLista();
			cl.setVisible(true);
		}
		else if (source == btReserva || source == miReserva) {
			ReservaPasseio rc = new ReservaPasseio();
			rc.setVisible(true);
			
		}
		else if (source == btRoteiro || source == miRoteiro) {
			RoteiroMenu rm = new RoteiroMenu();
			rm.setVisible(true);
		} 
                else if (source == btCancelarReserva || source == miCancelarReserva) {
                    CancelReservationForm crf = new CancelReservationForm();
                    crf.setVisible(true);                
		} else if(source == btPontoTuristico || source == miPontoTuristico) {
			PontoTuristicoMenu pm = new PontoTuristicoMenu();
			pm.setVisible(true);
		}
	}
}
