package br.fucapi.es.poo.solarium.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import br.fucapi.es.poo.solarium.controller.RoteiroController;
import br.fucapi.es.poo.solarium.entity.PontoTuristico;
import br.fucapi.es.poo.solarium.entity.Roteiro;



public class RoteiroMenu extends JFrame {

	private JPanel contentPane;
	private DefaultListModel<Roteiro> model;
	private RoteiroController controler = new RoteiroController();

	public RoteiroMenu() {
		setTitle("Roteiros");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
			
		model = new DefaultListModel();
		Set<Roteiro> roteiros = controler.buscaRoteiros();
			
		int i = 0;
		for (Roteiro roteiro : roteiros) {
			model.add(i, roteiro);
			i++;
		}
			
		final JList list = new JList(model);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setBounds(26, 42, 384, 121);
		contentPane.add(list);
			
		JButton btnVer = new JButton("Ver");
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = list.getSelectedIndex();
				if(model.size() > 0 && index >= 0){
					Roteiro roteiro = model.get(index);
					String texto = roteiro.getNome() + "\nDura��o Total: " + roteiro.getDuracao_total() + " minutos";
					for (PontoTuristico ponto : roteiro.getPontos()) {
						texto += "\n" + ponto.getDescricao();
					}
					JOptionPane.showMessageDialog(null, texto);					
				} else {
					JOptionPane.showMessageDialog(null, "Voc� deve selecionar um Roteiro!");
				}
			}
		});
		btnVer.setBounds(26, 174, 89, 23);
		contentPane.add(btnVer);
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int index = list.getSelectedIndex();
				if(model.size() > 0 && index >= 0) {
					Roteiro roteiro = model.getElementAt(index);
					RoteiroForm tela = new RoteiroForm(model,false,roteiro);
					tela.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Voc� deve selecionar um Roteiro!");
				}
			}
		});
		btnAlterar.setBounds(168, 174, 89, 23);
		contentPane.add(btnAlterar);
		
		JButton btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = list.getSelectedIndex();
				if(model.size() > 0 && index >= 0 && JOptionPane.showConfirmDialog(null, "Deseja mesmo excluir?") == 0) {
					Roteiro roteiro = model.getElementAt(index);
					controler.excluirRoteiro(roteiro);
					model.remove(index);					
				} else {
					JOptionPane.showMessageDialog(null, "Voc� deve selecionar um Roteiro!");
				}
			}
		});
		btnExcluir.setBounds(295, 174, 89, 23);
		contentPane.add(btnExcluir);
		
		JLabel lblPontosCadastrados = new JLabel("Roteiros cadastrados:");
		lblPontosCadastrados.setBounds(26, 17, 213, 14);
		contentPane.add(lblPontosCadastrados);
		
		JButton btnCadastrarNovo = new JButton("Cadastrar novo");
		btnCadastrarNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RoteiroForm tela = new RoteiroForm(model,true,null);
				tela.setVisible(true);
			}
		});
		btnCadastrarNovo.setBounds(128, 228, 167, 23);
		contentPane.add(btnCadastrarNovo);
		}
	}
