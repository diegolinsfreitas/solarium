package br.fucapi.es.poo.solarium.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import br.fucapi.es.poo.solarium.ReservaExistenteException;
import br.fucapi.es.poo.solarium.controller.ReservaControlador;
import br.fucapi.es.poo.solarium.entity.BandeiraCartao;
import br.fucapi.es.poo.solarium.entity.Cliente;
import br.fucapi.es.poo.solarium.entity.Passeio;

public class InformacaoReserva extends JDialog {

	private JPanel contentPane;
	private JTextField txtLocal;
	protected Passeio passeio;
	protected Cliente cliente,local;
	private JComboBox cbxBandeira;
	private JFormattedTextField formattedTextField;
	private JFormattedTextField txtHora;
	private JFormattedTextField txtValor;
	private NumberFormat instance = NumberFormat.getInstance(Locale.getDefault());;

	/**
	 * Create the frame.
	 * @param passeio 
	 * @param cliente 
	 * @throws ParseException 
	 */
	public InformacaoReserva(Cliente cliente, Passeio passeio) {
		this.cliente = cliente;
		this.passeio = passeio;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 418, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Local");
		lblNewLabel.setBounds(10, 11, 46, 14);
		contentPane.add(lblNewLabel);
		
		txtLocal = new JTextField();
		txtLocal.setBounds(10, 36, 389, 20);
		contentPane.add(txtLocal);
		txtLocal.setColumns(10);
		
		JLabel lblDatahora = new JLabel("Hora");
		lblDatahora.setBounds(10, 67, 63, 14);
		contentPane.add(lblDatahora);
		
		JLabel lblValor = new JLabel("Cart\u00E3o");
		lblValor.setBounds(10, 123, 46, 14);
		contentPane.add(lblValor);
		
		JLabel lblValor_1 = new JLabel("Valor");
		lblValor_1.setBounds(10, 177, 46, 14);
		contentPane.add(lblValor_1);
		
		JButton btnConfirmarReserva = new JButton("Confirmar Reserva");
		btnConfirmarReserva.setFont(new Font("Arial", Font.BOLD, 14));
		btnConfirmarReserva.setForeground(new Color(173, 255, 47));
		btnConfirmarReserva.setBackground(new Color(0, 100, 0));
		btnConfirmarReserva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InformacaoReserva.this.reservarPasseioAction();
				
			}
		});
		btnConfirmarReserva.setBounds(179, 239, 220, 23);
		contentPane.add(btnConfirmarReserva);

		try {
			txtHora = new JFormattedTextField(new MaskFormatter("##:##"));
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		txtHora.setBounds(10, 92, 390, 20);
		contentPane.add(txtHora);
		
		cbxBandeira = new JComboBox();
		for (BandeiraCartao bandeira : BandeiraCartao.values()) {
			cbxBandeira.addItem(bandeira);
		}
		cbxBandeira.setBounds(10, 148, 389, 22);
		contentPane.add(cbxBandeira);
		

		txtValor = new JFormattedTextField(instance);
	
		//txtValor.setComponentOrientation(java.awt.ComponentOrientation.RIGHT_TO_LEFT); 
		txtValor.setBounds(10, 202, 390, 20);
		contentPane.add(txtValor);
		
		JButton btnNewButton = new JButton("Cancelar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InformacaoReserva.this.fecharJanela();
			}
		});
		btnNewButton.setBounds(80, 239, 88, 23);
		contentPane.add(btnNewButton);
	}

	protected void reservarPasseioAction() {
		try {
			new ReservaControlador().realizarPagamento(
					this.passeio,
					this.cliente,
					this.txtLocal.getText(),
					this.getHoraReserva(),
					this.getBandeiraCartao(),
				    this.getValor()
				);
		} catch (ReservaExistenteException e) {
			JOptionPane.showMessageDialog(InformacaoReserva.this, "Ja existe um reserva para esse cliente e passeio");
			return;
		}
		
		this.fecharJanela();
		JOptionPane.showMessageDialog(this, "Reserva realizada com sucesso");
	}

	private BigDecimal getValor() {
		try {
			return BigDecimal.valueOf(instance.parse(this.txtValor.getText()).doubleValue());
		} catch (ParseException e) {
			throw new IllegalArgumentException("Valor da reserva inválida",e);
		}

	}

	private void fecharJanela() {
		WindowEvent windowClosing = new WindowEvent(InformacaoReserva.this, WindowEvent.WINDOW_CLOSING);
		InformacaoReserva.this.dispatchEvent(windowClosing);
	}

	private BandeiraCartao getBandeiraCartao() {
		return (BandeiraCartao) this.cbxBandeira.getSelectedItem();
	}
	
	private Calendar getHoraReserva(){
		Calendar hora = Calendar.getInstance();	
		Date parse;
		try {
			parse = new SimpleDateFormat("HH:mm").parse((String) this.txtHora.getValue());
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(this,"Hora informada é inválida");
			throw new RuntimeException(e);
		}
		hora.setTime(parse);
		return hora;
	}
}
