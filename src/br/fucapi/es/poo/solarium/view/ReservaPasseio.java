package br.fucapi.es.poo.solarium.view;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.fucapi.es.poo.solarium.controller.ClienteController;
import br.fucapi.es.poo.solarium.controller.PasseioControlador;
import br.fucapi.es.poo.solarium.entity.Cliente;
import br.fucapi.es.poo.solarium.entity.Passeio;
import br.fucapi.es.poo.solarium.repositorio.PasseioRepositorio;

public class ReservaPasseio extends JDialog {

	private JPanel contentPane;
	private JTable tblPasseio;
	private JButton btnBuscarClientes;
	private JTable tblClientes;
	private JButton btnNewButton;
	protected ArrayList<Passeio> listaPasseios;
	protected ArrayList<Cliente> listaClientes;

	/**
	 * Create the frame.
	 */
	public ReservaPasseio() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 667, 737);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnBuscarPasseios = new JButton("Buscar Passeios");
		btnBuscarPasseios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ReservaPasseio.this.carregarPasseios();
				atualizarTabelaPasseios(ReservaPasseio.this.listaPasseios);
			}

		

			private void atualizarTabelaPasseios(List<Passeio> passeios) {
				DefaultTableModel aModel = new DefaultTableModel() {
					// setting the jtable read only

					@Override
					public boolean isCellEditable(int row, int column) {
						return false;
					}
				};
				// setting the column name
				Object[] tableColumnNames = new Object[4];
				tableColumnNames[0] = "ID";
				tableColumnNames[1] = "Passeio";
				tableColumnNames[2] = "Data";
				tableColumnNames[3] = "Preco";
				
				aModel.setColumnIdentifiers(tableColumnNames);
				if (passeios == null) {
					ReservaPasseio.this.tblPasseio.setModel(aModel);
					return;
				}

				Object[] objects = new Object[4];
				Iterator<Passeio> itPasseios = passeios.iterator();
				// populating the tablemodel
				while (itPasseios.hasNext()) {
					Passeio passeio = itPasseios.next();
					objects[0] = passeio.getId();
					objects[1] = passeio.getRoteiro().getNome();
					objects[2] = passeio.getData().toString();
					objects[3] = passeio.getPrecoUnitario().toString();

					aModel.addRow(objects);
				}

				// binding the jtable to the model
				ReservaPasseio.this.tblPasseio.setModel(aModel);
			}
		});
		btnBuscarPasseios.setBounds(10, 11, 147, 23);
		contentPane.add(btnBuscarPasseios);

		tblPasseio = new JTable();
		tblPasseio.setBounds(10, 45, 639, 217);
		contentPane.add(tblPasseio);
		
		btnBuscarClientes = new JButton("Buscar Clientes");
		btnBuscarClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ReservaPasseio.this.carregarListaClientes();
			}
		});
		btnBuscarClientes.setBounds(10, 273, 166, 23);
		contentPane.add(btnBuscarClientes);
		
		tblClientes = new JTable();
		tblClientes.setBounds(10, 306, 639, 320);
		contentPane.add(tblClientes);
		
		btnNewButton = new JButton("Reservar Passeio");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int selectedRowCliente = ReservaPasseio.this.tblClientes.getSelectedRow();
				int selectedRowPasseio = ReservaPasseio.this.tblPasseio.getSelectedRow();
				if(selectedRowCliente <0 || selectedRowPasseio < 0){
					JOptionPane.showMessageDialog(ReservaPasseio.this, "Selecione um cliente e um passeio para a reserva");
					return;
				}
				
				InformacaoReserva informacaoReserva = new InformacaoReserva(
						ReservaPasseio.this.listaClientes.get(selectedRowCliente),
						ReservaPasseio.this.listaPasseios.get(selectedRowPasseio)
					);
				
				informacaoReserva.setVisible(true);
			}
		});
		btnNewButton.setBounds(199, 660, 245, 23);
		contentPane.add(btnNewButton);
		
		JButton btnClientes = new JButton("Clientes");
		btnClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ClienteLista clienteLista = new ClienteLista();
				clienteLista.setVisible(true);
				clienteLista.toFront();
				clienteLista.repaint();
				clienteLista.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent arg0) {
						ReservaPasseio.this.carregarListaClientes();
					}
				});
			}
		});
		btnClientes.setBounds(186, 273, 91, 23);
		contentPane.add(btnClientes);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PasseioControlador controller = new PasseioControlador();
				controller.exibirCadastro();
			}
		});
		btnCadastrar.setBounds(188, 11, 89, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnEmitirProgramacao = new JButton("Emitir Programacao");
		btnEmitirProgramacao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRowPasseio = ReservaPasseio.this.tblPasseio.getSelectedRow();
				if (selectedRowPasseio > -1) {
					Passeio passeio = listaPasseios.get(selectedRowPasseio);
					
					PasseioControlador pc = new PasseioControlador();
					pc.programarPasseio(passeio);
				}
			}
		});
		btnEmitirProgramacao.setBounds(287, 11, 125, 23);
		contentPane.add(btnEmitirProgramacao);
		
		this.setModalityType(Dialog.ModalityType.MODELESS);
	}
	
	private void carregarPasseios() {
		Set<Passeio> passeios = new PasseioControlador()
				.buscarPasseios();
		ReservaPasseio.this.listaPasseios = new ArrayList<Passeio>(passeios);
	}
	
	public void carregarListaClientes(){
		Collection<Cliente> clientes = new ClienteController().buscaClientes();
		ReservaPasseio.this.listaClientes = new ArrayList<Cliente>(clientes);
		DefaultTableModel aModel = new DefaultTableModel() {
			// setting the jtable read only

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		// setting the column name
		Object[] tableColumnNames = new Object[2];
		tableColumnNames[0] = "ID";
		tableColumnNames[1] = "Nome";

		aModel.setColumnIdentifiers(tableColumnNames);


		Object[] objects = new Object[2];
		Iterator<Cliente> itClientes = this.listaClientes.iterator();
		// populating the tablemodel
		while (itClientes.hasNext()) {
			Cliente cliente = itClientes.next();
			objects[1] = cliente.getNome();
			objects[0] = cliente.getId();

			aModel.addRow(objects);
		}

		// binding the jtable to the model
		this.tblClientes.setModel(aModel);
	}
}
