package br.fucapi.es.poo.solarium.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.fucapi.es.poo.solarium.entity.EstadoPasseio;
import br.fucapi.es.poo.solarium.entity.Passeio;
import br.fucapi.es.poo.solarium.entity.Roteiro;
import br.fucapi.es.poo.solarium.repositorio.PasseioRepositorio;
import java.awt.GridLayout;
/*import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;*/

public class PasseioCadastro
		extends JFrame
		implements ActionListener {
	
	private JComboBox<Roteiro> cbRoteiro; 
	private JButton btSalvar, btCancelar;
	private JTextField tfData, tfPreco;
	
	
	final String format = "K:mm DD/MM/YYYYY";
	private JLabel label_9;
	private JLabel label_10;
	
	public PasseioCadastro(List<Roteiro> roteiro) {
		cbRoteiro = new JComboBox(roteiro.toArray());
		
		JLabel lbRoteiro = new JLabel("Roteiro: ");
		
		JPanel content = new JPanel();
/*		content.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("92px"),
				ColumnSpec.decode("82px"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(38dlu;default)"),
				ColumnSpec.decode("max(30dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC,},
			new RowSpec[] {
				FormFactory.NARROW_LINE_GAP_ROWSPEC,
				RowSpec.decode("24px"),
				RowSpec.decode("max(5dlu;default)"),
				RowSpec.decode("29px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("27px"),
				RowSpec.decode("10px"),
				RowSpec.decode("27px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));*/
		content.add(lbRoteiro, "1, 2, fill, fill");
		content.add(cbRoteiro, "2, 2, 4, 1, fill, fill");
		JLabel lbData = new JLabel("Data do passeio");
		content.add(lbData, "1, 4, fill, fill");
		
		tfData = new JTextField("HH:MM dd/mm/yyyy", 20);
		content.add(tfData, "2, 4, 4, 1, fill, fill");
		JLabel lbPreco = new JLabel("Preco Unitario: ");
		content.add(lbPreco, "1, 6, fill, fill");
		tfPreco = new JTextField("0.0", 20);
		content.add(tfPreco, "2, 6, 4, 1, fill, fill");
		
		this.setTitle("Cadastro Passeio");
		this.setContentPane(content);
		
		label_9 = new JLabel("");
		content.add(label_9, "1, 7, fill, fill");
		
		label_10 = new JLabel("");
		content.add(label_10, "2, 7, fill, fill");
		btCancelar = new JButton("cancelar");
		btCancelar.addActionListener(this);
		
		btSalvar = new JButton("salvar");
		
		btSalvar.addActionListener(this);
		content.add(btSalvar, "2, 8, fill, fill");
		content.add(btCancelar, "4, 8, fill, fill");
		this.setSize(320, 185);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		
		if (source == btSalvar) {
			SimpleDateFormat df = new SimpleDateFormat(format);
			try {
				Date data = df.parse(tfData.getText());
				Roteiro roteiro = (Roteiro) cbRoteiro.getSelectedItem();
				BigDecimal preco = new BigDecimal(tfPreco.getText());
				
				if (cbRoteiro.getSelectedIndex() < 0) {
					throw new Exception("roteiro nao selecionado");
				}
				
				Passeio p = new Passeio(null, data, preco, EstadoPasseio.ABERTO, roteiro, null, null, null, null);
				PasseioRepositorio repo = new PasseioRepositorio();
				repo.salvar(p);
				
				JOptionPane.showMessageDialog(this, "Passeio cadastrado com sucesso", "Cadastro Sucesso", 
						JOptionPane.INFORMATION_MESSAGE);
				this.setVisible(false);
				
			} catch (ParseException e) {
				JOptionPane.showMessageDialog(this, "input error", "Erro", 
						JOptionPane.ERROR_MESSAGE);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Selecione um roteiro", "Erro", 
						JOptionPane.ERROR_MESSAGE);
				
			}
		}
		else if (source == btCancelar) {
			this.setVisible(false);
		}
		
	}

}
