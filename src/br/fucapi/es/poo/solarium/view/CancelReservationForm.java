package br.fucapi.es.poo.solarium.view;

import br.fucapi.es.poo.solarium.controller.ClienteController;
import br.fucapi.es.poo.solarium.controller.ReservaControlador;
import br.fucapi.es.poo.solarium.entity.Cliente;
import br.fucapi.es.poo.solarium.entity.Reserva;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author sifilho
 */

public class CancelReservationForm extends JDialog {
    private JPanel mainPanel;
    private JTextField customerName;
    private JTable reservationTable;
    private JButton searchButton;
    private JButton cancelButton;
       
    private ClienteController customerController;
    private ReservaControlador reservationController;
    
    ArrayList<Reserva> reservations;

    public CancelReservationForm() {
        initComponents();
        customerController = new ClienteController();
        reservationController = new ReservaControlador();
    }
    
    private void initComponents() {
        // Login Form 
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Cancelar Reserva");
        setModal(true);
        setBounds(100, 100, 500, 400);
        
        // Main Panel
        
        mainPanel =  new JPanel();
        mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        mainPanel.setLayout(null);
        setContentPane(mainPanel);
        
        // Customer Label
        
        JLabel userLabel = (new JLabel("Cliente"));
        userLabel.setBounds(10, 11, 46, 14);
        mainPanel.add(userLabel);        
        
        // User Text Field
        
        customerName = new JTextField();
        customerName.setBounds(10, 25, 305, 25);
        customerName.setColumns(10);
        mainPanel.add(customerName);    
        
        // Action Listener

        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source == searchButton) {
                    CancelReservationForm.this.searchCustomer();
                } else if (source == cancelButton) {
                    CancelReservationForm.this.doCancelation();
                    CancelReservationForm.this.searchCustomer();
                }
            }
        };
        
        // Search Button
        
        searchButton = new JButton("Localizar Reservas");
        searchButton.setBounds(320, 25, 150, 25);
        searchButton.addActionListener(actionListener);
        mainPanel.add(searchButton);        
        
        // Reservation Table Label

        JLabel tableLabel = (new JLabel("Lista de Reservas"));
        tableLabel.setBounds(10, 50, 150, 14);
        mainPanel.add(tableLabel);           
        
        // Reservation Table
        
        reservationTable = new JTable();
        reservationTable.setBounds(10, 64, 460, 250);
        mainPanel.add(reservationTable);        
        
        // Cancel Button
        
        cancelButton = new JButton("Cancelar Reserva");
        cancelButton.setBounds(320, 325, 150, 25);
        cancelButton.addActionListener(actionListener);
        mainPanel.add(cancelButton);                
    }

    private void searchCustomer() {
        Cliente cliente = customerController.buscaClientePorNome(customerName.getText());
        if (cliente == null) {
            JOptionPane.showMessageDialog(null, "Cliente não encontrado.");
        } else {
            List<Reserva> reservations = cliente.getReservas();
            if (reservations == null || reservations.isEmpty()) {
                JOptionPane.showMessageDialog(null, "O Cliente informado não possui reservas.");
            } else {
                showSearchResult(reservations);
            }
        }
    }

    private void showSearchResult(List<Reserva> collection) {
        // Create Table Model

        DefaultTableModel tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        Object[] tableColumnNames = new Object[3];
        tableColumnNames[0] = "Id";
        tableColumnNames[1] = "Passeio";
        tableColumnNames[2] = "Local";
        tableModel.setColumnIdentifiers(tableColumnNames);
        reservationTable.setModel(tableModel);

        // Fill Table Model
        
        this.reservations = new ArrayList(collection);
        Object[] row = new Object[3];
        Iterator<Reserva> itReservations = this.reservations.iterator();		
        while (itReservations.hasNext()) {
            Reserva reservation = itReservations.next();
            row[0] = reservation.getId();
            row[1] = reservation.getPasseio().getRoteiro().getNome();
            row[2] = reservation.getLocal();
            tableModel.addRow(row);
        }
    }

    private void doCancelation() {
        if (this.reservations == null) {
            JOptionPane.showMessageDialog(null, "Consulte um cliente com Reservas.");
        } else if (reservationTable.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Selecione antes uma Reserva da lista.");
        } else {
            Object reservationId = reservationTable.getValueAt(reservationTable.getSelectedRow(), 0);
            if (reservationId != null) {
                Iterator<Reserva> itReservations = this.reservations.iterator();		
                while (itReservations.hasNext()) {
                    Reserva reservation = itReservations.next();
                    if (reservationId.equals(reservation.getId()) ) {
                        try {
                            reservationController.cancelaReserva(reservation);
                            JOptionPane.showMessageDialog(null, "Cancelamento realizado com sucesso.");
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Falha no cancelamento: " +
                                    ex.getMessage());
                        }
                        break;
                    }
                }
            }
        }   
    }
}