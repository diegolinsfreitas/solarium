package br.fucapi.es.poo.solarium.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import br.fucapi.es.poo.solarium.controller.ClienteController;


public class ClienteFormulario extends JDialog{

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtCpf;
	private JTextField txtPassaporte;


	/**
	 * Create the frame.
	 */
	public ClienteFormulario() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModal(true);
		setBounds(100, 100, 438, 247);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(10, 11, 46, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 36, 409, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setBounds(10, 67, 46, 14);
		contentPane.add(lblCpf);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(10, 92, 409, 20);
		contentPane.add(txtCpf);
		txtCpf.setColumns(10);
		
		JLabel lblPassaporte = new JLabel("Passaporte");
		lblPassaporte.setBounds(10, 123, 78, 14);
		contentPane.add(lblPassaporte);
		
		txtPassaporte = new JTextField();
		txtPassaporte.setBounds(10, 144, 409, 20);
		contentPane.add(txtPassaporte);
		txtPassaporte.setColumns(10);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ClienteFormulario.this.salvarClienteAction();
			}
		});
		btnSalvar.setBounds(330, 175, 89, 23);
		contentPane.add(btnSalvar);
	}


	protected void salvarClienteAction() {
		ClienteController clienteController = new ClienteController();
		if(clienteController.validaCliente(this.txtNome.getText())){
			int showConfirmDialog = JOptionPane.showConfirmDialog(ClienteFormulario.this, "Salvar Novo Cliente?");
			if(showConfirmDialog == 0){
				clienteController.incluirCliente(
						this.txtNome.getText(), this.txtCpf.getText(), this.txtPassaporte.getText());
				JOptionPane.showMessageDialog(this, "Cliente Salvo com sucesso");
				fecharJanela();
			}
		}else{
			JOptionPane.showMessageDialog(this, "");
		}
		
		
		
	}
	
	private void fecharJanela() {
		WindowEvent windowClosing = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
		this.dispatchEvent(windowClosing);
	}
}
