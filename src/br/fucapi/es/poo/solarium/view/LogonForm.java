package br.fucapi.es.poo.solarium.view;

import br.fucapi.es.poo.solarium.controller.AuthenticationCallback;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author sifilho
 */
public class LogonForm extends JDialog {
    private AuthenticationCallback callback;
    private JPanel mainPanel;
    private JTextField userTextField;
    private JPasswordField passwordTextField;
    private JButton logonButton;
    private JButton cancelButton;

    public LogonForm(AuthenticationCallback callback) {
        if (callback == null)
            throw new IllegalArgumentException("callback parameter cannot be null");
        
        this.callback = callback;
        initComponents();
    }

    private void initComponents() {
        
        // Login Form 
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle("Sistema Solarium");
        setModal(true);
        setBounds(100, 100, 340, 180);
        
        // Main Panel
        
        mainPanel =  new JPanel();
        mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        mainPanel.setLayout(null);
        setContentPane(mainPanel);
        
        // User Label
        
        JLabel userLabel = (new JLabel("Usuário"));
        userLabel.setBounds(80, 11, 46, 14);
        mainPanel.add(userLabel);
        
        // User Text Field
        
        userTextField = new JTextField();
        userTextField.setBounds(80, 25, 150, 25);
        userTextField.setColumns(10);
        mainPanel.add(userTextField);        
        
        // Password Label

        JLabel passwordLabel = (new JLabel("Senha"));
        passwordLabel.setBounds(80, 50, 46, 14);
        mainPanel.add(passwordLabel);     

        // Password Text Field
        
        passwordTextField = new JPasswordField();
        passwordTextField.setBounds(80, 64, 150, 25);
        passwordTextField.setColumns(8);
        mainPanel.add(passwordTextField);        

        // Action Listener

        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object source = e.getSource();
                if (source == logonButton) {
                    LogonForm.this.doCallback();
                } else if (source == cancelButton) {
                    close();
                }
            }
        };
        
        // Login Button
        
        logonButton = new JButton("Efetuar Logon");
        logonButton.setBounds(10, 100, 150, 30);
        logonButton.addActionListener(actionListener);
        mainPanel.add(logonButton);
        
        // Cancel Button
        
        cancelButton = new JButton("Cancelar");
        cancelButton.setBounds(165, 100, 150, 30);
        cancelButton.addActionListener(actionListener);
        mainPanel.add(cancelButton);
    }

    private void doCallback() {
        if (callback.authenticate(userTextField.getText(), new String(passwordTextField.getPassword()))) {
            close();
        } else {
            JOptionPane.showMessageDialog(null, "Usuário ou senha inválidos.");
        }
    }    
    
    private void close() {
        WindowEvent windowClosing = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        this.dispatchEvent(windowClosing);
    }
}
