package br.fucapi.es.poo.solarium;

import br.fucapi.es.poo.solarium.controller.Authentication;
import java.awt.EventQueue;
import javax.swing.JFrame;
import br.fucapi.es.poo.solarium.view.MenuPrincipal;

public class Solarium {
    /**
        * @param args
        */
    public static void main(String[] args) {
        PersistenceUtils.getEM();
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    if ((new Authentication()).logon()) {
                        JFrame f = new MenuPrincipal();
                        f.setVisible(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
