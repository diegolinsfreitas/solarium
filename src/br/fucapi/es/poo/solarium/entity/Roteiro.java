package br.fucapi.es.poo.solarium.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@EqualsAndHashCode public class Roteiro {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Getter @Setter private Long id;
	@Getter @Setter private String nome;
	@Getter @Setter private int duracao_total;
	@OneToMany
	@Getter @Setter private List<PontoTuristico> pontos;
	
	public String toString() {
		return nome;
	}
}
