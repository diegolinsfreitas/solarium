package br.fucapi.es.poo.solarium.entity;

public enum EstadoPasseio {
	ABERTO, FECHADO, CANCELADO, CONCLUIDO
}
