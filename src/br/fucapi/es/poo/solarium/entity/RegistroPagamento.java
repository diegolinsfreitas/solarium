package br.fucapi.es.poo.solarium.entity;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
@Entity
public class RegistroPagamento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Getter	private BigDecimal valor;
	@Getter private String idTransacao;
	@Getter @NonNull private BandeiraCartao cartao;

	public RegistroPagamento() {
		
	}
	
	public RegistroPagamento(BigDecimal valor, BandeiraCartao bandeiraCartao) {
		if (bandeiraCartao == null) {
			throw new NullPointerException("bandeira is null");
		}
		if (valor.compareTo(BigDecimal.ZERO) < 0) {
			throw new IllegalArgumentException("valor is negative");
		}
		
		
		this.idTransacao = UUID.randomUUID().toString();
		this.cartao = bandeiraCartao;
		this.valor = valor;
	}

}
