package br.fucapi.es.poo.solarium.entity;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;

import lombok.Data;

@Entity
@Data public class Cliente {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@OrderBy()
	private String nome;
	private String cpf;
	private String passaporte;
	
	@ManyToMany(fetch= FetchType.EAGER ,mappedBy="cliente")
	private List<Reserva> reservas;
}
