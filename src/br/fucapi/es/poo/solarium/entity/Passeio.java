package br.fucapi.es.poo.solarium.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.sun.istack.internal.Nullable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Entity
@AllArgsConstructor
@Data public class Passeio {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private Date data;
	private BigDecimal precoUnitario;
	private EstadoPasseio estado;
	
	@ManyToOne() @NonNull
	private Roteiro roteiro;
	@ManyToOne() @Nullable
	private Guia guia;
	@ManyToOne() @Nullable
	private Motorista motorista;
	@ManyToOne() @Nullable
	private Veiculo veiculo;
	
	@ManyToMany()
	private List<Reserva> reservas;

	Passeio(){
		
	}
	
	public String toString() {
		return roteiro.getNome() + ": " + data.toLocaleString();
		
	}

}
