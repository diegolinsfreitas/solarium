package br.fucapi.es.poo.solarium.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data public class Veiculo {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String placa;
	private String fabricante;
	private String modelo;
	private int locacaoMaxima;
	private float consumoMedio;
	private float capacidadeTanque;
}
