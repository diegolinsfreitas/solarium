package br.fucapi.es.poo.solarium.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;


@Entity
@EqualsAndHashCode public class PontoTuristico {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Getter @Setter private long id;
	@Getter @Setter private String descricao;
	@Getter @Setter private String endereco;
	@Getter @Setter private int duracao;
	
	public String toString() {
		return descricao;
	}
}
