package br.fucapi.es.poo.solarium.entity;

import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
@Entity
public class Reserva implements Comparable<Reserva> {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@ManyToOne
	@NonNull private Cliente cliente;
	@ManyToOne
	@NonNull private Passeio passeio;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@NonNull private RegistroPagamento pagamento;
	
	@NonNull private String local;
	
	
	@NonNull private Calendar hora;

        public Reserva() {
        }
	
	//comprovanteReserva e comprovanteEstorno omitidos
	//
	
	@Override
	public int compareTo(Reserva that) {
		if (this.equals(that) || (cliente.equals(that.cliente) &&
				passeio.equals(that.passeio) &&	id == that.id)) {
			return 0;
		}
		return (id > that.id) ? 1 : -1;
	}



	
}
