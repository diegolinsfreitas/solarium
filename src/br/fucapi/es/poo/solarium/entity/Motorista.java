package br.fucapi.es.poo.solarium.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data public class Motorista {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String nome;
	private String cpf;
	private String carteira;
	private Date dataNascimento;
	
	@OneToMany()
	private List<Passeio> passeios;
}
