package br.fucapi.es.poo.solarium;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceUtils {
	static EntityManagerFactory FACTORY;
	static{
		FACTORY = Persistence.createEntityManagerFactory("solariumpu");
	}
	
	public static EntityManager getEM(){
		return PersistenceUtils.FACTORY.createEntityManager();
	}

}
