package br.fucapi.es.poo.solarium.repositorio;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.entity.PontoTuristico;

public class PontoTuristicoRepositorio {
	
	public Set<PontoTuristico> buscaPontos() {
		Collection<PontoTuristico> resultList = PersistenceUtils.getEM().createQuery("from PontoTuristico").getResultList();
		return  new HashSet(resultList);
	}

	public void excluirPonto(PontoTuristico ponto) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.remove(em.getReference(PontoTuristico.class, ponto.getId()));		
		transaction.commit();
	}

	public void cadastrarPonto(PontoTuristico ponto) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.persist(ponto);		
		transaction.commit();
	}
	
	public void alterarPonto(PontoTuristico ponto) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		em.merge(ponto);		
		transaction.commit();		
	}
	

}
