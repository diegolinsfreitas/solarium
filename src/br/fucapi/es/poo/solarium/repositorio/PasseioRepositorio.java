package br.fucapi.es.poo.solarium.repositorio;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.entity.Cliente;
import br.fucapi.es.poo.solarium.entity.Passeio;

public class PasseioRepositorio {
	public Set<Passeio> buscar() {
		Collection<Passeio> resultList =
				PersistenceUtils.getEM().createQuery("from Passeio").getResultList();
		return  new HashSet(resultList);
	}
	
	public Passeio buscar(long id) {
		return PersistenceUtils.getEM().find(Passeio.class, id); 
	}
	
	public void salvar(Passeio passeio) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		
		transaction.begin();
		em.merge(passeio);
		transaction.commit();
	}
	
	public void adicionar(Passeio passeio) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		
		transaction.begin();
		em.persist(passeio);
		transaction.commit();
		
	}
}
