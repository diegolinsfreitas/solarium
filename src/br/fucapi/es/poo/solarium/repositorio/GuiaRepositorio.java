package br.fucapi.es.poo.solarium.repositorio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.entity.Guia;

public class GuiaRepositorio {
	public List<Guia> buscar() {
		EntityManager em = PersistenceUtils.getEM();
		
		return em.createQuery("From Guia").getResultList();
	}
	
	public Guia buscar(long id) {
		EntityManager em = PersistenceUtils.getEM();
		
		return em.find(Guia.class, id);
	}
	
	public void salvar(Guia guia) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction t = em.getTransaction();
		
		t.begin();
		em.persist(guia);
		t.commit();
		
	}
	
	public void remover(Guia guia) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction t = em.getTransaction();
		
		t.begin();
		em.remove(guia);
		t.commit();
	}

}
