package br.fucapi.es.poo.solarium.repositorio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.entity.Guia;
import br.fucapi.es.poo.solarium.entity.Veiculo;

public class VeiculoRepositorio {
	public List<Veiculo> buscar() {
		EntityManager em = PersistenceUtils.getEM();
		
		return em.createQuery("From Veiculo").getResultList();
	}
	
	public Veiculo buscar(long id) {
		EntityManager em = PersistenceUtils.getEM();
		
		return em.find(Veiculo.class, id);
	}
	
	public void salvar(Veiculo veiculo) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction t = em.getTransaction();
		
		t.begin();
		em.persist(veiculo);
		t.commit();
		
	}
	
	public void remover(Veiculo veiculo) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction t = em.getTransaction();
		
		t.begin();
		em.remove(veiculo);
		t.commit();
	}
}
