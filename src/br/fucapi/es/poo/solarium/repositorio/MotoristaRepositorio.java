package br.fucapi.es.poo.solarium.repositorio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.entity.Guia;
import br.fucapi.es.poo.solarium.entity.Motorista;

public class MotoristaRepositorio {
	public List<Motorista> buscar() {
		EntityManager em = PersistenceUtils.getEM();
		
		return em.createQuery("From Motorista").getResultList();
	}
	
	public Motorista buscar(long id) {
		EntityManager em = PersistenceUtils.getEM();
		
		return em.find(Motorista.class, id);
	}
	
	public void salvar(Motorista motorista) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction t = em.getTransaction();
		
		t.begin();
		em.persist(motorista);
		t.commit();
		
	}
	
	public void remover(Motorista motorista) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction t = em.getTransaction();
		
		t.begin();
		em.remove(motorista);
		t.commit();
	}
}
