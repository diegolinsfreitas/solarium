package br.fucapi.es.poo.solarium.repositorio;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.ReservaExistenteException;
import br.fucapi.es.poo.solarium.entity.Cliente;
import br.fucapi.es.poo.solarium.entity.Reserva;
import java.util.Collection;
import javax.persistence.NoResultException;

public class ReservaRepositorio {
	

    public void guardarReserva(Reserva reserva) throws ReservaExistenteException {
            EntityManager em = PersistenceUtils.getEM();
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            em.persist(reserva);

            transaction.commit();
    }
    
    public void deletaReserva(Reserva reserva) {
        EntityManager em = PersistenceUtils.getEM();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        em.remove(em.getReference(Reserva.class, reserva.getId()));
        transaction.commit();
    }
}
