package br.fucapi.es.poo.solarium.repositorio;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.entity.Cliente;
import javax.persistence.NoResultException;

public class ClienteRepositorio {

	public void guardarCliente(Cliente cliente) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		
		em.persist(cliente);
		
		transaction.commit();
	}

	public Collection<Cliente> buscaCliente() {
		Collection<Cliente> resultList =
				PersistenceUtils.getEM()
					.createQuery("from Cliente ORDER BY nome")
					.getResultList();
		return  resultList;
	}

        public Cliente buscaClientePorNome(String nome) {
            Object result = null;
            try {
                result = PersistenceUtils.getEM()
                        .createQuery("from Cliente where nome = :nome")
                        .setParameter("nome", nome)
                        .getSingleResult();
            } catch (NoResultException ex) {
                ex.printStackTrace();
            }

            Cliente cliente = null;
            if (result != null) {
                cliente = (Cliente)result;
            }
            return cliente;              
        }
}
