package br.fucapi.es.poo.solarium.repositorio;

import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.entity.User;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

/**
 *
 * @author sifilho
 */
public class UserDAO {
    
    public User getUserByName(String userName) {
        Object result = null;
        try {
            result = PersistenceUtils.getEM()
                     .createQuery("from User where userName = :name")
                     .setParameter("name", userName)
                     .getSingleResult();
        } catch (NoResultException ex) {
            ex.printStackTrace();
        }
        
        User user = null;
        if (result != null) {
            user = (User)result;
        }
        return user;    
    }
    
    public void insert(User user) {
        EntityTransaction transaction = PersistenceUtils.getEM().getTransaction();
        transaction.begin();		
        PersistenceUtils.getEM().persist(user);		
        transaction.commit();        
    }
}
