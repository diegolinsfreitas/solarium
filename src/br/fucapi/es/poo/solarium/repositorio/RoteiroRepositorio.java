package br.fucapi.es.poo.solarium.repositorio;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.fucapi.es.poo.solarium.PersistenceUtils;
import br.fucapi.es.poo.solarium.entity.PontoTuristico;
import br.fucapi.es.poo.solarium.entity.Roteiro;

public class RoteiroRepositorio {

	public void cadastrarRoteiro(Roteiro roteiro) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();		
		em.persist(roteiro);		
		transaction.commit();
	}
	
	public void alterarRoteiro(Roteiro roteiro) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();		
		em.merge(roteiro);		
		transaction.commit();
	}
	
	public Set<Roteiro> buscaRoteiros() {
		Collection<PontoTuristico> resultList = PersistenceUtils.getEM().createQuery("from Roteiro").getResultList();
		return  new HashSet(resultList);
	}

	public void excluirRoteiro(Roteiro roteiro) {
		EntityManager em = PersistenceUtils.getEM();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();		
		em.remove(em.getReference(Roteiro.class, roteiro.getId()));		
		transaction.commit();		
	}

}
