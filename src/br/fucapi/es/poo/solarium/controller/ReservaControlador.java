package br.fucapi.es.poo.solarium.controller;

import java.math.BigDecimal;
import java.util.Calendar;

import br.fucapi.es.poo.solarium.ReservaExistenteException;
import br.fucapi.es.poo.solarium.entity.*;
import br.fucapi.es.poo.solarium.repositorio.ReservaRepositorio;
import java.util.Collection;

public class ReservaControlador {

	private ReservaRepositorio reservaRepositorio = new ReservaRepositorio();

	public void realizarPagamento(Passeio passeio, Cliente cliente,
			String local, Calendar dataHora, BandeiraCartao bandeiraCartao, BigDecimal valor) throws ReservaExistenteException {
		RegistroPagamento registroPagamento = confirmaPagamento(bandeiraCartao,
				valor);
		this.criaReserva(passeio,  cliente,
			 local,  dataHora, registroPagamento);
	}

	private RegistroPagamento confirmaPagamento(BandeiraCartao bandeiraCartao,
			BigDecimal valor) {
		RegistroPagamento registroPagamento = new RegistroPagamento(valor, bandeiraCartao);
		return registroPagamento;
	}

	private void criaReserva(Passeio passeio, Cliente cliente,
			String local, Calendar dataHora, RegistroPagamento registroPagamento) throws ReservaExistenteException {
		Reserva reserva = new Reserva(cliente, passeio, registroPagamento,local,dataHora);
		reservaRepositorio.guardarReserva(reserva);
		
	}
        
        public void cancelaReserva(Reserva reserva) throws Exception {
            if (reserva.getPasseio() == null) {
                reservaRepositorio.deletaReserva(reserva);
            } else if (reserva.getPasseio().getEstado() == EstadoPasseio.ABERTO ||
                       reserva.getPasseio().getEstado() == EstadoPasseio.CANCELADO) {
                reservaRepositorio.deletaReserva(reserva);
            } else {
                throw new Exception("A Reserva não pode ser mais cancelada.");
            }
        }
}
