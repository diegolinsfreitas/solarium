package br.fucapi.es.poo.solarium.controller;

import java.util.ArrayList;
import java.util.Set;

import javax.swing.JFrame;

import br.fucapi.es.poo.solarium.entity.Passeio;
import br.fucapi.es.poo.solarium.repositorio.GuiaRepositorio;
import br.fucapi.es.poo.solarium.repositorio.MotoristaRepositorio;
import br.fucapi.es.poo.solarium.repositorio.PasseioRepositorio;
import br.fucapi.es.poo.solarium.repositorio.RoteiroRepositorio;
import br.fucapi.es.poo.solarium.repositorio.VeiculoRepositorio;
import br.fucapi.es.poo.solarium.view.PasseioCadastro;
import br.fucapi.es.poo.solarium.view.ProgramacaoPasseioFormulario;

public class PasseioControlador {

	private PasseioRepositorio passeioRepositorio = new PasseioRepositorio();
	
	public Set<Passeio> buscarPasseios() {
		return passeioRepositorio.buscar();
	}
	

	public void programarPasseio(Passeio passeio) {
		GuiaRepositorio      repoGuia = new GuiaRepositorio();
		MotoristaRepositorio repoMotorista = new MotoristaRepositorio();
		VeiculoRepositorio   repoVeiculo = new VeiculoRepositorio();
		
		JFrame form = new ProgramacaoPasseioFormulario(
				passeio, repoGuia.buscar(), repoMotorista.buscar(), repoVeiculo.buscar());
		
		form.setVisible(true);
	}
	
	public void exibirCadastro() {
		JFrame f = new PasseioCadastro(
				new ArrayList((new RoteiroRepositorio()).buscaRoteiros()));
		
		f.setVisible(true);
		
	}
	
	public void salvar(Passeio passeio) {
		passeioRepositorio.salvar(passeio);
	}
}
