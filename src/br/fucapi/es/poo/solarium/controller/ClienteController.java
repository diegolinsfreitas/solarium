package br.fucapi.es.poo.solarium.controller;

import java.util.Collection;

import br.fucapi.es.poo.solarium.entity.Cliente;
import br.fucapi.es.poo.solarium.repositorio.ClienteRepositorio;

public class ClienteController {
	
	private ClienteRepositorio repositorioCliente = new ClienteRepositorio();
	
	public boolean validaCliente(String nome){
		if(nome == null || "".equals(nome.trim())){
			return false;
		}
		return true;
	}
	
	public void incluirCliente(String nome,String cpf, String passaporte){
		
		Cliente cliente = new Cliente();
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		cliente.setPassaporte(passaporte);
		
		repositorioCliente.guardarCliente(cliente);
	}

	public Collection<Cliente> buscaClientes() {
		return repositorioCliente.buscaCliente();
	}
        
        public Cliente buscaClientePorNome(String nome){
            return repositorioCliente.buscaClientePorNome(nome);
        } 
	
}
