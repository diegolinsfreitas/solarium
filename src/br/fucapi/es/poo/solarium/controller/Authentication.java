package br.fucapi.es.poo.solarium.controller;

import br.fucapi.es.poo.solarium.entity.User;
import br.fucapi.es.poo.solarium.repositorio.UserDAO;
import br.fucapi.es.poo.solarium.view.LogonForm;

/**
 *
 * @author sifilho
 */

public class Authentication implements AuthenticationCallback { 
    
    private boolean isAuthenticated;
    
    public boolean logon() {
        isAuthenticated = false;
        LogonForm logonForm = new LogonForm(this);
        logonForm.setVisible(true);
        return isAuthenticated;        
    }

    @Override
    public boolean authenticate(String userName, String password) {
        User user = (new UserDAO()).getUserByName(userName);
        isAuthenticated = (user != null && user.getPassword().equals(password));
        //isAuthenticated = true;
        return isAuthenticated;        
    }
}
