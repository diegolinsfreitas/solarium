package br.fucapi.es.poo.solarium.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListModel;

import br.fucapi.es.poo.solarium.entity.PontoTuristico;
import br.fucapi.es.poo.solarium.entity.Roteiro;
import br.fucapi.es.poo.solarium.repositorio.RoteiroRepositorio;

public class RoteiroController {
	private RoteiroRepositorio repo = new RoteiroRepositorio();
	
	public Roteiro cadastrarRoteiro(final DefaultListModel<PontoTuristico> model, String nome) {
		Roteiro roteiro = new Roteiro();
		roteiro.setPontos(asList(model));
		roteiro.setNome(nome);
		roteiro.setDuracao_total(getDuracaoTotal(model));
		repo.cadastrarRoteiro(roteiro);
		return roteiro;
	}
	
	public Set<Roteiro> buscaRoteiros() {
		return repo.buscaRoteiros();
	}
	
	public void alterarRoteiro(Roteiro roteiro, final DefaultListModel<PontoTuristico> model, String nome) {
		roteiro.setNome(nome);
		roteiro.setPontos(asList(model));
		roteiro.setDuracao_total(getDuracaoTotal(model));
		repo.alterarRoteiro(roteiro);
	}
	
	public void excluirRoteiro(Roteiro roteiro) {
		repo.excluirRoteiro(roteiro);
	}
	
	
	private int getDuracaoTotal(final DefaultListModel<PontoTuristico> model) {
		int tam = model.size();
		int duracao_total = 0;
		for(int i = 0; i < tam; i++) {
			duracao_total += model.getElementAt(i).getDuracao();
		}		
		return duracao_total;
	}
	
	private List<PontoTuristico> asList(final DefaultListModel<PontoTuristico> model) {
		List<PontoTuristico> lista = new ArrayList<PontoTuristico>();
		int tam = model.size();
		for(int i = 0; i < tam ; i++) {
			lista.add(model.getElementAt(i));
		}
		return lista;
	}

}
