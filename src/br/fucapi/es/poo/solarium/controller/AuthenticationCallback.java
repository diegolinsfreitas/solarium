package br.fucapi.es.poo.solarium.controller;

/**
 *
 * @author sifilho
 */

public interface AuthenticationCallback {
    boolean authenticate(String userName, String password);
}
