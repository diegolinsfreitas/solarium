package br.fucapi.es.poo.solarium.controller;

import java.util.Set;

import br.fucapi.es.poo.solarium.entity.PontoTuristico;
import br.fucapi.es.poo.solarium.repositorio.PontoTuristicoRepositorio;

public class PontoTuristicoControlador {
	private PontoTuristicoRepositorio repo = new PontoTuristicoRepositorio();
	
	public Set<PontoTuristico> buscaPontos() {
		return repo.buscaPontos();
	}

	public void excluirPonto(PontoTuristico ponto) {
		repo.excluirPonto(ponto);		
	}
	
	public PontoTuristico cadastrarPonto(String descricao, String endereco, int duracao) {
		PontoTuristico ponto = new PontoTuristico();
		ponto.setDescricao(descricao);
		ponto.setDuracao(duracao);
		ponto.setEndereco(endereco);
		repo.cadastrarPonto(ponto);
		return ponto;
	}

	public void alterarPonto(PontoTuristico ponto, String descricao, String endereco, int duracao) {
		ponto.setDescricao(descricao);
		ponto.setDuracao(duracao);
		ponto.setEndereco(endereco);
		repo.alterarPonto(ponto);
	}

}
